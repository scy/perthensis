#!/bin/sh
set -e

# Perthensis Project Skeleton build.sh
# see <https://codeberg.org/scy/perthensis> for more information.

me='build.sh'

# Output a prefixed message.
msg() {
	printf '%s: %s\n' "$me" "$*"
}

# Output a prefixed message to stderr and exit.
err() {
	rc="$1"
	shift
	msg "$*" >&2
	exit "$rc"
}

# Check whether a certain environment variable exists. If it doesn't, die.
require_var() {
	var="$1"
	shift
	if [ -z "$(eval "printf '%s' \"\$$var\"")" ]; then
		msg="$*"
		[ -z "$msg" ] || msg=" $msg"
		err 1 "Required environment variable $var is missing.$msg If you run build.sh using Pipenv, make sure your .env file is set up correctly."
	fi
}

# Run rsync with some default parameters.
do_rsync() {
	rsync -Lav --exclude __pycache__ "$@"
}

# Run a build using the given build map. Each parameter has to be a `from>to`
# pair, which means the usual PERTHENSIS_BUILD_MAP env variable, which is
# colon-separated, has to be split using $IFS magic or something.
do_build() {
	for pair in "$@"; do
		from="$(printf '%s' "$pair" | cut -d '>' -f 1)"
		to="$(printf '%s' "$pair" | cut -d '>' -f 2)"
		do_rsync "./$from/" "./$PERTHENSIS_BUILD_OUT/$to"
	done
}

# Output usage information.
usage() {
	cat <<-END
Usage: $me [-hfbdr]

Options are listed in the order their respective action would take place.

  -h  Display this help message and exit.
  -f  Flash MicroPython to the connected board. This will delete everything on
      the board! The firmware binary will be downloaded if not present already.
  -b  Assemble all source files into $PERTHENSIS_BUILD_OUT/.
  -d  Deploy the contents of $PERTHENSIS_BUILD_OUT/ to the connected board.
  -r  Connect to a Python REPL on the board.
	END
}


# Default values for some environment variables, especially to be able to show
# their value in usage().
: ${PERTHENSIS_BUILD_OUT:='build'}
: ${PERTHENSIS_MICROPYTHON_DIR:='micropython'}


# Option parsing.
flash='n'
build='n'
deploy='n'
repl='n'
while getopts ':hfbdr' arg; do
	case "$arg" in
		f)
			flash='y'
			;;
		b)
			build='y'
			;;
		d)
			deploy='y'
			;;
		h)
			usage
			exit
			;;
		r)
			repl='y'
			;;
		'?')
			msg "Unknown option: -$OPTARG" >&2
			usage >&2
			exit 1
			;;
	esac
done


# (Attempt to) make sure we run from the right directory. This comes _after_
# option parsing to allow people to ash for "-h" from anywhere.
if ! [ -e "$me" -a -e Pipfile ]; then
	err 1 "This tool should run from the top level of your MicroPython project, i.e. the one that contains files like \"$me\" and \"Pipfile\"."
fi


# Check whether at least one action has been selected at all.
if [ "$flash" = 'n' -a "$build" = 'n' -a "$deploy" = 'n' -a "$repl" = 'n' ]; then
	msg 'You should probably select at least one action.' >&2
	usage >&2
	exit
fi


# Flash MicroPython.
if [ "$flash" = 'y' ]; then
	require_var PERTHENSIS_CHIP 'Set it to a chip model supported by esptool, e.g. "esp32".'
	require_var PERTHENSIS_FLASH_ADDR 'Set it to the firmware flashing offset for esptool.'
	require_var PERTHENSIS_MICROPYTHON_VERSION 'Check <https://micropython.org/download/ for available versions. Set the variable to just the base filename _without_ the .bin extension, e.g. "esp32-idf3-20200902-v1.13".'
	mkdir -p "$PERTHENSIS_MICROPYTHON_DIR"
	curl -L -o "$PERTHENSIS_MICROPYTHON_DIR/$PERTHENSIS_MICROPYTHON_VERSION.bin" "https://micropython.org/resources/firmware/$PERTHENSIS_MICROPYTHON_VERSION.bin"
	esptool.py --chip "$PERTHENSIS_CHIP" erase_flash
	esptool.py --chip "$PERTHENSIS_CHIP" write_flash "$PERTHENSIS_FLASH_ADDR" "$PERTHENSIS_MICROPYTHON_DIR/$PERTHENSIS_MICROPYTHON_VERSION.bin"
fi


# Gather files into build directory.
if [ "$build" = 'y' ]; then
	require_var PERTHENSIS_BUILD_MAP 'It should be set to a colon-separated list of from>to pairs. See env.example for an example.'
	rm -rf "./$PERTHENSIS_BUILD_OUT"
	IFS=':' command eval 'do_build $PERTHENSIS_BUILD_MAP'
fi


# Collect all rshell commands to that we only need to call it once. This saves
# a lot of time because of rshell's initialization that runs first.
rshell_cmd=''

# Sync build directory to board.
if [ "$deploy" = 'y' ]; then
	[ -n "$rshell_cmd" ] && rshell_cmd="$rshell_cmd ; "
	rshell_cmd="${rshell_cmd}rsync -m "$PERTHENSIS_BUILD_OUT" /pyboard"
fi

# Connect to REPL.
if [ "$repl" = 'y' ]; then
	[ -n "$rshell_cmd" ] && rshell_cmd="$rshell_cmd ; "
	rshell_cmd="${rshell_cmd}repl"
fi

# If we have any rshell commands, run them.
[ -n "$rshell_cmd" ] && rshell $rshell_cmd

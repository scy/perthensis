# Perthensis Project Skeleton

The [Perthensis](https://codeberg.org/scy/perthensis) MicroPython library provides this project template for your custom MicroPython project.
Simply copy all files you find in this directory (`project-skeleton`) to the top level of your own project.
Don't forget `.gitignore`!
Also, be sure to customize this readme to your project's needs.

Note that this template is currently geared towards ESP32 and ESP8266 chips.

If you're reading this from within a MicroPython project that is _not_ Perthensis, chances are that someone created the project based on the Perthensis template, included this readme file and didn't customize it.

## Setting up the project

The Perthensis template is using Pipenv to install and interact with esptool and rshell.
The following paragraphs should guide you through setting up your development environment.

### `.env` file

Even though every developer on a project should be working with more or less the same _source code_, their local _environment_ will be different.
For example, on one dev's machine the MicroPython board will be available for communication using the `/dev/ttyUSB0` device, on another machine it might be `/dev/ttyS6`.

One of the best ways to customize these settings is to use environment variables.
If you're an advanced user, you can set them any way you like, but one of the easiest ways is to write them into a file called `.env`.
Pipenv will automatically load that file and set the variables contained in it.

See [`env.example`](env.example) for how such a file can look like.
Copy it to `.env` (that's the full file name) and customize it to your machine.

It's important to **not** add that file to Git:
After all, these are local settings for your machine, and they're not useful to other devs or might even interfere with their workflow.

### Pipenv

You need a development environment that has [Pipenv](https://pipenv.pypa.io/) installed.
We'll be using it to install `esptool` and `rshell`.

If you don't have Pipenv yet, install it using a command like `sudo apt install pipenv` or `pip3 install --user pipenv` or whatever your local machine requires.
Refer to the [Pipenv installation docs](https://pipenv.pypa.io/en/latest/install/) if you run into trouble.

### Non-Python libraries

`esptool` requires the Python `cryptography` library, which in turn might depend on the `ffi` and `openssl` libraries.

Your system might be smart enough to install them automatically.
If it's not, `pipenv install` below will fail.
You can then try to install the libraries manually, e.g. using `sudo apt install libffi-dev libssl-dev`.

### Installing esptool and rshell

These are the two tools required to flash MicroPython on your board and afterwards interact with it.

Since the previous paragraphs prepared your system, installing them should be as easy as running `pipenv install` now.

## Using the build script

`build.sh` can be used as a template for your own needs, but it's also customizable enough that you possibly don't need to modify it at all.
It can

* download and flash MicroPython
* assemble files from several directories (e.g. your own code combined with Perthensis and other libraries) into a single one
* use `rshell rsync` to put that directory onto your board
* launch a REPL on the board

Use the command-line options to choose which of these actions to perform.
See `./build.sh -h` for a list of all the options.

Note that the script depends on some of the environment variables listed in [`env.example`](env.example) and is supposed to run in the Pipenv environment, i.e. via `pipenv run ./build.sh`.
If you're using an `.env` file to set the environment variables, Pipenv will automatically set them to the values you specified.

You can also edit the `Pipfile` to add some convenience scripts in the `[scripts]` section.
Some examples are already provided there.
Note that the part after the equals sign _always_ needs to be wrapped in double quotes.

You can call these scripts using `pipenv run NAME OPTIONS`.
For example, by default, we alias `build` to `./build.sh`, so you can execute `pipenv run build -r` to call `build.sh` and ask it to open a REPL.

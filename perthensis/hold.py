"""React to a signal being held in a certain state for some time."""


class Hold:
    """Watch a signal using a scheduler and fire events when it stays constant
    for a certain amount of time."""

    def __init__(self, callback, timeout, resolution=50, retrigger=False):
        """Initialize a new hold watcher.

        Args:
            callback (callable): Function to be called once the timeout has
                been reached. It will receive this ``Hold`` instance as its
                first and the signal value as its second parameter.
            timeout (int): If the signal has been constant for at least this
                number of milliseconds, trigger the callback.
            resolution (int): How long should we wait (in milliseconds) between
                checks for whether the timeout has occured? If your timeout is
                several seconds long, you can increase this value to save CPU.
        """
        self._cb = callback
        self._timeout = self._countdown = int(timeout)
        self._res = int(resolution)
        self._retrig = bool(retrigger)
        self._trigger = True
        self._value = None

    @property
    def value(self):
        """The value to track. Can be any valid Python value, but it's best to
        keep it simple. This value defaults to ``None``, and whenever you
        change it, the timeout countdown will start over.

        Note that if you pass a mutable type like a list, changes to that
        list will _not_ be picked up and do _not_ count as a change.
        """
        return self._value

    @value.setter
    def value(self, new):
        if self._value == new:
            # Nothing has changed.
            return
        self._countdown = self._timeout
        self._trigger = True  # Definitely resume triggering.
        self._value = new

    async def watch(self, sch):
        """Watch for the timeout to expire.

        Pass this function to ``create_task`` of Perthensis' ``Scheduler``.
        """
        res = self._res

        while True:
            if self._countdown <= 0:
                if self._trigger:
                    self._trigger = self._retrig  # Pause if no retriggers.
                    self._cb(self, self._value)
            else:
                self._countdown -= res
            await sch.sleep_ms(res)

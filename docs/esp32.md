# ESP32 Notes & Hints

## Which pins to use?

_This is based on [Andreas Spiess’s video on the topic](https://youtu.be/LY-1DHTxRAk)._

| GPIO | Rcm | Out | RTC | ADC | DAC | Touch | UART | I²C | SPI    | JTAG | Notes                                                 |
| ---: | :-: | :-: | :-: | :-: | :-: | :---: | ---: | :-- | :----- | :--- | :----                                                 |
|    0 |     |  •  |  •  |  2  |     |   •   |      |     |        |      | will trigger firmware upload when pulled low on boot  |
|    1 |     |  •  |     |     |     |       |  TX0 |     |        |      |                                                       |
|    2 |     |  •  |  •  |  2  |     |   •   |      |     |        |      | will prevent firmware upload when pulled high on boot |
|    3 |     |  •  |     |     |     |       |  RX0 |     |        |      |                                                       |
|    4 |  •  |  •  |  •  |  2  |     |   •   |      |     |        |      |                                                       |
|    5 |     |  •  |     |     |     |       |      |     | V-CS   |      | has some “SDIO timing” secondary function(?)          |
| 6…11 |     |     |     |     |     |       |      |     |        |      | usually not available (on-board flash)                |
|   12 |     |  •  |  •  |  2  |     |   •   |      |     | H-MISO | MTDI | must not be high during boot?                         |
|   13 |     |  •  |  •  |  2  |     |   •   |      |     | H-MOSI | MTCK |                                                       |
|   14 |     |  •  |  •  |  2  |     |   •   |      |     | H-CLK  | MTMS |                                                       |
|   15 |     |  •  |  •  |  2  |     |   •   |      |     | H-CS   | MTDO | prevents boot-up UART log when pulled low on boot     |
|   16 |     |  •  |     |     |     |       |  RX2 |     |        |      | unavailable on WROVER (PSRAM)                         |
|   17 |     |  •  |     |     |     |       |  TX2 |     |        |      | unavailable on WROVER (PSRAM)                         |
|   18 |     |  •  |     |     |     |       |      |     | V-CLK  |      |                                                       |
|   19 |     |  •  |     |     |     |       |      |     | V-MISO |      |                                                       |
|   21 |     |  •  |     |     |     |       |      | SDA |        |      |                                                       |
|   22 |     |  •  |     |     |     |       |      | SCL |        |      |                                                       |
|   23 |     |  •  |     |     |     |       |      |     | V-MOSI |      |                                                       |
|   25 |  •  |  •  |  •  |  2  |  •  |       |      |     |        |      |                                                       |
|   26 |  •  |  •  |  •  |  2  |  •  |       |      |     |        |      |                                                       |
|   27 |  •  |  •  |  •  |  2  |     |   •   |      |     |        |      |                                                       |
|   32 |  •  |  •  |  •  |  1  |     |   •   |      |     |        |      |                                                       |
|   33 |  •  |  •  |  •  |  1  |     |   •   |      |     |        |      |                                                       |
|   34 |     |     |  •  |  1  |     |       |      |     |        |      | input only, no pull-up or pull-down                   |
|   35 |     |     |  •  |  1  |     |       |      |     |        |      | input only, no pull-up or pull-down                   |
|   36 |     |     |  •  |  1  |     |       |      |     |        |      | Hall sensor VE, input only, no pull-up or pull-down   |
|   39 |     |     |  •  |  1  |     |       |      |     |        |      | Hall sensor VN, input only, no pull-up or pull-down   |

* Pins marked “Rcm” are what we recommend for true general-purpose I/O because they don’t have secondary functions. Note that only some of them support ADC, DAC or touch.
* All output pins support PWM.
* All pins marked “RTC” (which includes all “touch” pins) support waking up the ESP from deep sleep using interrupts.
* ADC2 cannot be used together with WLAN.
* There are two SPI ports named HSPI and VSPI. The SPI column uses the respective prefix.
